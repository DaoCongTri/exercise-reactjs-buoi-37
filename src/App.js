import Form from "./ExerciseReactJS/Form";
export default function App() {
  return (
    <div className="app">
      <Form />
    </div>
  );
}
