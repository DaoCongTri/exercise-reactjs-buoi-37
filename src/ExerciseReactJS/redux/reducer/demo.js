import { HANDLEONBLUR, HANDLEONCHANGE, SUBMIT } from "../constant/constants";
const initialState = {
  values: {
    id: "",
    name: "",
    number: "",
    email: "",
  },
  errors: {
    id: "",
    name: "",
    number: "",
    email: "",
  },
  listStudent: [
    {
      id: "1",
      name: "Nguyễn Văn A",
      number: "090346785",
      email: "nguyenvana@gmail.com",
    },
  ],
};
export const demo = (state = initialState, { type, payload }) => {
  switch (type) {
    case HANDLEONCHANGE: {
      const { id, value } = payload.target;
      let error = "";
      // id
      if (id === "id") {
        const reg = /^[0-9]{1,5}$/;
        if (!reg.test(value)) {
          error = `Mã sinh viên phải là số và khoảng từ 1 - 5 chữ số`;
        } else {
          error = ``;
        }
      }
      // name
      if (id === "name") {
        const nameRegex = /^[a-zA-Z ]+$/;
        if (nameRegex.test(value)) {
          error = ``;
        } else {
          error = `Họ tên sinh viên phải là chữ`;
        }
      }
      // number
      if (id === "number") {
        const reg = /^[0-9]{9,11}$/;
        if (!reg.test(value)) {
          error = `Số điện thoại phải là số và khoảng từ 9 - 11 chữ số`;
        } else {
          error = ``;
        }
      }
      if (id === "email") {
        const reg = /^[A-Za-z0-9]+@([\w-]+\.)+[\w-]{2,4}$/;
        if (!reg.test(value)) {
          error = `Email sinh viên phải đúng định dạng`;
        } else {
          error = ``;
        }
      }
      if (value.trim() === "") {
        error = `Trường này không được để trống`;
      }
      return {
        ...state,
        values: {
          ...state.values,
          [id]: value,
        },
        errors: {
          ...state.errors,
          [id]: error,
        },
      };
    }
    case HANDLEONBLUR: {
      const { id, value } = payload.target;
      let error = "";
      // id
      if (id === "id") {
        const reg = /^[0-9]{1,5}$/;
        if (!reg.test(value)) {
          error = `Mã sinh viên phải là số và khoảng từ 1 - 5 chữ số`;
        } else {
          error = ``;
        }
      }
      // name
      if (id === "name") {
        const nameRegex = /^[a-zA-Z ]+$/;
        if (nameRegex.test(value)) {
          error = ``;
        } else {
          error = `Họ tên sinh viên phải là chữ`;
        }
      }
      // number
      if (id === "number") {
        const reg = /^[0-9]{9,11}$/;
        if (!reg.test(value)) {
          error = `Số điện thoại phải là số và khoảng từ 9 - 11 chữ số`;
        } else {
          error = ``;
        }
      }
      if (id === "email") {
        const reg = /^[A-Za-z0-9]+@([\w-]+\.)+[\w-]{2,4}$/;
        if (!reg.test(value)) {
          error = `Email sinh viên phải đúng định dạng`;
        } else {
          error = ``;
        }
      }
      if (value.trim() === "") {
        error = `Trường này không được để trống`;
      }
      return {
        ...state,
        values: {
          ...state.values,
          [id]: value,
        },
        errors: {
          ...state.errors,
          [id]: error,
        },
      };
    }
    case SUBMIT: {
      payload.preventDefault();
      const { values, errors } = state;
      let isValid = true;
      for (const key in values) {
        if (values[key] === "" || errors[key] !== "") {
          isValid = false;
        }
      }
      const index = state.listStudent.findIndex((item) => {
        if (item.id === state.values.id) {
          return true;
        }
      });
      if (index !== -1) {
        isValid = false;
      }
      if (!isValid) {
        console.log("không cho submit");
        return { ...state };
      }
      console.log("submit");
      const newStudent = [...state.listStudent, state.values];
      state.listStudent = newStudent;
      return { ...state };
    }
    default:
      return state;
  }
};
