import React, { Component } from "react";
import { connect } from "react-redux";
import { viewDelete, viewEdit, viewSearch } from "./redux/action/studentAction";
class List extends Component {
  render() {
    let { data } = this.props;
    return (
      <div className="container">
        <div className="row bg-dark mb-3">
          <h1 className="col-6 text-white pl-4">Danh sách sinh viên</h1>
          <div className="col-6">
            <div className="row">
              <div className="col-10 p-2">
                <input
                  type="text"
                  id="valueSearch"
                  name="valueSearch"
                  className="form-control"
                  placeholder="Nhập họ tên sinh viên cần tìm"
                  onChange={(event) => {
                    this.props.handleSearch(event.target.value);
                  }}
                />
              </div>
              <div className="col-2 p-2">
                <button
                  className="btn btn-dark"
                  // onClick={(event) => {
                  //   this.props.handleSearch(event.target.value);
                  // }}
                >
                  <i
                    className="fas fa-search fa-lg"
                    style={{ color: "white" }}
                  ></i>
                </button>
              </div>
            </div>
          </div>
        </div>

        <table border={1} className="table">
          <thead className="text-center">
            <tr>
              <th>Mã SV</th>
              <th>Họ tên SV</th>
              <th>SĐT</th>
              <th>Email</th>
              <th>Tùy chọn</th>
            </tr>
          </thead>
          <tbody>
            {data.map((item) => {
              let { id, name, number, email } = item;
              return (
                <tr key={id}>
                  <td>{id}</td>
                  <td>{name}</td>
                  <td>{number}</td>
                  <td>{email}</td>
                  <td>
                    <button
                      className="btn btn-primary mr-3"
                      onClick={() => {
                        this.props.handleEdit(id);
                      }}
                    >
                      Chỉnh sửa
                    </button>
                    <button
                      className="btn btn-danger"
                      onClick={() => {
                        this.props.handleDelete(id);
                      }}
                    >
                      Xóa
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    data: state.StudentReducer.listStudent,
  };
};
let mapDispatchToState = (dispatch) => {
  return {
    handleDelete: (event) => {
      dispatch(viewDelete(event));
    },
    handleEdit: (event) => {
      dispatch(viewEdit(event));
    },
    handleSearch: (event) => {
      dispatch(viewSearch(event));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToState)(List);
